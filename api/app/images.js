const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Image = require('../models/Image');
const Place = require('../models/Place');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const images = await Image.find();

    return res.status(200).send(images);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/places/:id', async (req, res) => {
  try {
    const place = await Place.findById(req.params.id);
    const images = await Image.find({ place: place._id });

    if (images) {
      return res.status(200).send(images);
    } else {
      return res.status(404).send({ error: 'Photos not found' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.post('/places/:id', [auth, upload.single('image')], async (req, res) => {
  const imageData = {
    title: req.body.title,
    author: req.user,
    place: req.params.id,
  };

  if (req.file) {
    imageData.image = `uploads/${req.file.filename}`;
  }

  const image = new Image(imageData);

  console.log(image);

  try {
    await image.save();
    res.status(200).send(image);
  } catch (error) {
    return res.status(400).send(error);
  }
});

// router.delete("/:id", auth, async (req, res) => {
//     try {
//         const photo = await Photo.findOne({_id: req.params.id});

//         if (photo.author.equals(req.user._id)) {
//             photo.delete();
//         }

//         return res.send(photo);
//     } catch (e) {
//         res.status(500).send(e);
//     }
// });

module.exports = router;
