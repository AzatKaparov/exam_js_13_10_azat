const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Place = require('../models/Place');
const Image = require('../models/Image');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const places = await Place.find();

    return res.status(200).send(places);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const place = await Place.findById(req.params.id);

    if (place) {
      return res.status(200).send(place);
    } else {
      return res.status(404).send({ error: 'Place not found' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
  const placeData = {
    owner: req.user,
    title: req.body.title,
    description: req.body.description | '',
  };

  if (req.file) {
    placeData.image = `uploads/${req.file.filename}`;
  }

  const place = new Place(placeData);

  const image = new Image({
    image: place.image,
    author: req.user,
    place: place._id,
  });

  try {
    await place.save();
    await image.save();
    res.status(200).send(place);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const photo = await Photo.findOne({ _id: req.params.id });

    if (photo.author.equals(req.user._id)) {
      photo.delete();
    }

    return res.send(photo);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
