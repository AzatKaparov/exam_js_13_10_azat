const express = require('express');
const Review = require('../models/Review');
const Place = require('../models/Place');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const photos = await Review.find();

    return res.status(200).send(photos);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/places/:id', async (req, res) => {
  try {
    const place = await Place.findById(req.params.id);

    const reviews = await Review.find({ place: place._id })
      .populate('author', 'displayName')
      .sort({ date: -1 });

    if (reviews) {
      return res.status(200).send(reviews);
    } else {
      return res.status(404).send({ error: 'Photos not found' });
    }
  } catch (e) {
    res.send(e);
  }
});

router.post('/', auth, async (req, res) => {
  const reviewData = {
    text: req.body.text,
    author: req.user,
    place: req.body.place,
    service: req.body.service,
    kitchen: req.body.kitchen,
    interior: req.body.interior,
  };

  const review = new Review(reviewData);

  try {
    await review.save();
    res.status(200).send(review);
  } catch (error) {
    return res.status(400).send(error);
  }
});

module.exports = router;
