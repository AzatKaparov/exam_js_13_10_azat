const mongoose = require('mongoose');
const { nanoid } = require('nanoid');
const config = require('./config');
const User = require('./models/User');
const Place = require('./models/Place');
const Review = require('./models/Review');
const Image = require('./models/Image');

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2, user3, user4] = await User.create(
    {
      email: 'admin@admin.com',
      password: 'admin',
      token: nanoid(),
      displayName: 'Admin',
    },
    {
      email: 'root@root.com',
      password: 'root',
      token: nanoid(),
      displayName: 'Root',
    },
    {
      email: 'user@user.com',
      password: 'user',
      token: nanoid(),
      displayName: 'User',
    },
    {
      email: 'azat@azat.com',
      password: 'azat',
      token: nanoid(),
      displayName: 'Azat',
    },
  );

  const [place1, place2, place3] = await Place.create(
    {
      owner: user1,
      image: 'fixtures/img-1.jpg',
      title: 'Square',
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
    {
      owner: user2,
      image: 'fixtures/img-2.jpg',
      title: 'Park',
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
    {
      owner: user3,
      image: 'fixtures/img-3.jpg',
      title: 'Some water place',
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    },
  );

  await Review.create(
    {
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      author: user2,
      place: place1,
      service: 5,
      kitchen: 4,
      interior: 4,
    },
    {
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      author: user3,
      place: place1,
      service: 3,
      kitchen: 2,
      interior: 4,
    },
    {
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      author: user2,
      place: place2,
      service: 4,
      kitchen: 5,
      interior: 5,
    },
    {
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      author: user1,
      place: place2,
      service: 1,
      kitchen: 2,
      interior: 3,
    },
    {
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      author: user3,
      place: place3,
      service: 3,
      kitchen: 4,
      interior: 5,
    },
    {
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
      author: user1,
      place: place1,
      service: 4,
      kitchen: 2,
      interior: 4,
    },
  );

  await Image.create(
    {
      image: 'fixtures/img-4.jpg',
      author: user2,
      place: place1,
    },
    {
      image: 'fixtures/img-5.jpg',
      author: user2,
      place: place1,
    },
    {
      image: 'fixtures/img-6.jpg',
      author: user3,
      place: place2,
    },
    {
      image: 'fixtures/img-7.jpg',
      author: user1,
      place: place2,
    },
  );

  await mongoose.connection.close();
};

run().catch(console.error);
