require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const users = require('./app/users');
const places = require('./app/places');
const reviews = require('./app/reviews');
const images = require('./app/images');

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/places', places);
app.use('/reviews', reviews);
app.use('/images', images);

const run = async () => {
    await mongoose.connect('mongodb://localhost/final');

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', async () => {
        console.log('exiting');
        await mongoose.disconnect();
    });
};

run().catch(e => console.error(e));