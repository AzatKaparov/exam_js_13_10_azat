const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const ImageShema = new Shema({
    image: {
        type: String,
        required: true,
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    place: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Place',
    },
});

const Image = mongoose.model('Image', ImageShema);
module.exports = Image;