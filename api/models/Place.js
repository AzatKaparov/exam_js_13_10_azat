const mongoose = require('mongoose');
const Shema = mongoose.Schema;
const Image = require('./Image');

const PlaceShema = new Shema({
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  image: {
    type: String,
    ref: 'Image',
    required: true,
  },
  reviews: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Review',
    },
  ],
  rating: {
    type: Number,
    default: 0,
    required: true,
  },
});

PlaceShema.pre('save', async function (next) {
  await Image.create({
    image: this.image,
    author: this.owner._id,
    place: this._id,
  });

  next();
});

const Place = mongoose.model('Place', PlaceShema);
module.exports = Place;
