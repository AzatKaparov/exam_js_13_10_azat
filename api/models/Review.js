const mongoose = require('mongoose');
const Shema = mongoose.Schema;
const Place = require('./Place');

const ReviewShema = new Shema({
  text: {
    type: String,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Place',
    required: true,
  },
  service: {
    type: Number,
    max: 5,
    required: true,
  },
  kitchen: {
    type: Number,
    max: 5,
    required: true,
  },
  interior: {
    type: Number,
    max: 5,
    required: true,
  },
  date: {
    type: Date,
    default: new Date(+new Date() + 7 * 24 * 60 * 60 * 1000),
  },
});

ReviewShema.pre('save', async function (next) {
  await Place.findOneAndUpdate(
    { _id: this.place._id },
    {
      $push: {
        reviews: this,
      },
    },
  );

  next();
});

const Review = mongoose.model('Review', ReviewShema);
module.exports = Review;
