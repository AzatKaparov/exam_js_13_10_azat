import { Redirect, Route, Switch } from 'react-router-dom';
import Layout from './components/UI/Layout/Layout';
import Register from './containers/Register/Register';
import Login from './containers/Login/Login';
import Home from './containers/Home/Home';
import { useSelector } from 'react-redux';
import AddPlace from './containers/AddPlace/AddPlace';
import Place from './containers/Place/Place';

const App = () => {
  const user = useSelector((state) => state.users.user);

  const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
    return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
  };

  return (
    <div className="App">
      <Layout>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/register" exact component={Register} />
          <Route path="/login" exact component={Login} />
          <Route path="/place/:id" exact component={Place} />
          <Route path="/add-place" exact component={AddPlace} />
          <Route render={() => <h1>Not found</h1>} />
        </Switch>
      </Layout>
    </div>
  );
};

export default App;
