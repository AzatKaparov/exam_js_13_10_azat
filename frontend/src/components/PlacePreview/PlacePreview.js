import React from 'react';
import PropTypes from 'prop-types';
import { Card, Col } from 'react-bootstrap';
import { API_URL } from '../../constants';
import './PlacePreview.css';
import { NavLink } from 'react-router-dom';

const PlacePreview = ({ img, title, id, rating, imagesCount, reviewsCount }) => {
  return (
    <Col xl={4} className="mb-4">
      <Card>
        <Card.Img className="PlacePreviewImage" variant="top" src={`${API_URL}/${img}`} />
        <Card.Body>
          <Card.Title>
            <NavLink to={`/place/${id}`}>{title}</NavLink>
          </Card.Title>
          <Card.Text>Rating: {rating}</Card.Text>
          <Card.Text>Photos: {imagesCount}</Card.Text>
          <Card.Text>Reviews: {reviewsCount}</Card.Text>
        </Card.Body>
      </Card>
    </Col>
  );
};

PlacePreview.propTypes = {
  img: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  rating: PropTypes.number,
  imagesCount: PropTypes.number,
  reviewsCount: PropTypes.number,
};

export default PlacePreview;
