import React from 'react';
import moment from 'moment';
import { getOverallRating } from '../../containers/Place/Place';
import './ReviewItem.css';

const ReviewItem = ({ author, ratings, text, date }) => {
  return (
    <div className="ReviewItem my-2 border p-2">
      <p>
        {moment(date).format('d MMM YYYY')} by <span className="author">{author.displayName}</span>
      </p>
      <h6>{text}</h6>
      <ul>
        <li>
          <b>Overall: {getOverallRating(ratings)}</b>
        </li>
        <li>Quality of food: {ratings.kitchen}</li>
        <li>Service quality: {ratings.service}</li>
        <li>Interior: {ratings.interior}</li>
      </ul>
    </div>
  );
};

export default ReviewItem;
