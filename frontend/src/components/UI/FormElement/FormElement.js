import React from 'react';
import { Form } from 'react-bootstrap';
import PropTypes from 'prop-types';

const FormElement = ({
  onChangeHandler,
  name,
  value,
  type,
  placeholder,
  required,
  label,
  file,
  error,
  isTextArea,
}) => {
  if (file) {
    return (
      <Form.Group>
        <Form.Label>{label}</Form.Label>
        <Form.Control
          type={type}
          name={name}
          required={required}
          onChange={onChangeHandler}
          isInvalid={!!error}
        />
        <Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>
      </Form.Group>
    );
  } else if (isTextArea) {
    return (
      <Form.Group className="mb-3">
        <Form.Label>{label}</Form.Label>
        <Form.Control
          as="textarea"
          style={{ height: '200px' }}
          onChange={onChangeHandler}
          name={name}
          value={value ? value : ''}
          required={required}
          type={type}
          placeholder={placeholder}
          isInvalid={!!error}
        />
        <Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>
      </Form.Group>
    );
  }

  return (
    <Form.Group className="mb-3">
      <Form.Label>{label}</Form.Label>
      <Form.Control
        onChange={onChangeHandler}
        name={name}
        value={value ? value : ''}
        required={required}
        type={type}
        placeholder={placeholder}
        isInvalid={!!error}
      />
      <Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>
    </Form.Group>
  );
};

FormElement.propTypes = {
  onChangeHandler: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  label: PropTypes.string.isRequired,
  file: PropTypes.bool,
  isTextArea: PropTypes.bool,
};

export default FormElement;
