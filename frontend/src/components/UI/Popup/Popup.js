import React from 'react';
import {Modal, Button} from "react-bootstrap";
import './Popup.css'

const Popup = ({ show, img, hide }) => {
    return (
        <Modal
            show={show}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header className="d-flex justify-content-end">
                <Button onClick={hide}>Close</Button>
            </Modal.Header>
            <Modal.Body className="d-flex justify-content-center">
                <img className="PopupImage" src={img}/>
            </Modal.Body>
        </Modal>
    );
};

export default Popup;