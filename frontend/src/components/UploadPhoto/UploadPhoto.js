import React, { useDebugValue, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import FormElement from '../../components/UI/FormElement/FormElement';
import { addImage } from '../../store/actions/imagesActions';

const UploadPhoto = ({id, toggleUpload}) => {
  const dispatch = useDispatch();
  const [options, setOptions] = useState({
    image: '',
  });

  const onFileChange = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setOptions((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const onFormSubmit = async (imageData) => {
    await dispatch(addImage(id, imageData));
    toggleUpload();
  };

  const submitFormHandler = (e) => {
    e.preventDefault();
    const formData = new FormData();

    Object.keys(options).forEach((key) => {
      formData.append(key, options[key]);
    });
    onFormSubmit(formData).catch((err) => console.error(err));
  };

  return (
    <Form onSubmit={submitFormHandler}>
      <FormElement
        label="Upload image"
        name="image"
        onChangeHandler={onFileChange}
        type="file"
        required
        file
      />
      <Button className="my-3 btn-lg" type="submit" variant="success">
        Create
      </Button>
    </Form>
  );
};

export default UploadPhoto;
