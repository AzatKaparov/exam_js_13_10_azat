import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import FormElement from '../../components/UI/FormElement/FormElement';
import { useDispatch, useSelector } from 'react-redux';
import Preloader from '../../components/UI/Preloader/Preloader';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import { getFieldError } from '../../constants';
import { createPlace } from '../../store/actions/placesActions';

const AddPlace = () => {
  const dispatch = useDispatch();
  const { placeError, placeLoading } = useSelector((state) => state.places);
  const [options, setOptions] = useState({
    title: '',
    image: '',
    description: '',
  });

  const inputChangeHandler = (e) => {
    const { name, value } = e.target;
    setOptions((prevState) => ({ ...prevState, [name]: value }));
  };

  const onFileChange = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setOptions((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const onFormSubmit = async (placeData) => {
    await dispatch(createPlace(placeData));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();
    const formData = new FormData();

    Object.keys(options).forEach((key) => {
      formData.append(key, options[key]);
    });
    onFormSubmit(formData).catch((err) => console.error(err));
  };

  return (
    <>
      <Preloader show={placeLoading} />
      <Backdrop show={placeLoading} />
      <div className="container">
        TEST
        <Form onSubmit={submitFormHandler}>
          <FormElement
            label="Title"
            name="title"
            value={options.title}
            onChangeHandler={inputChangeHandler}
            type="text"
            placeholder="Title"
            error={getFieldError(placeError, 'title')}
            required
          />
          <FormElement
            label="Description"
            name="description"
            value={options.description}
            onChangeHandler={inputChangeHandler}
            type="text"
            placeholder="Description"
            error={getFieldError(placeError, 'description')}
            isTextArea
          />
          <FormElement
            label="Image"
            name="image"
            onChangeHandler={onFileChange}
            type="file"
            error={getFieldError(placeError, 'image')}
            required
            file
          />
          <Button className="my-3 btn-lg" type="submit" variant="success">
            Create
          </Button>
        </Form>
      </div>
    </>
  );
};

export default AddPlace;
