import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPlaces } from '../../store/actions/placesActions';
import { Row } from 'react-bootstrap';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import Preloader from '../../components/UI/Preloader/Preloader';
// import Popup from "../../components/UI/Popup/Popup";
import PlacePreview from '../../components/PlacePreview/PlacePreview';
import { fetchImages } from '../../store/actions/imagesActions';
import { fetchReviews } from '../../store/actions/reviewsActions';

const Home = () => {
  const dispatch = useDispatch();
  const { places, photosLoading } = useSelector((state) => state.places);
  const [modal, setModal] = useState({
    show: null,
  });
  const { images } = useSelector((state) => state.images);
  const { reviews } = useSelector((state) => state.reviews);

  const showModalHandler = (img) => {
    setModal({
      img: img,
      show: true,
    });
  };

  const hideModalHandler = () => {
    setModal({
      ...modal,
      show: false,
    });
  };

  useEffect(() => {
    dispatch(fetchPlaces());
    dispatch(fetchImages());
    dispatch(fetchReviews());
  }, [dispatch]);

  return (
    <>
      {/* <Popup show={modal.show} img={modal.img} hide={hideModalHandler} /> */}
      <Backdrop show={modal.show} onClick={() => hideModalHandler()} />
      <Preloader show={photosLoading} />
      <Backdrop show={photosLoading} />
      <div className="container">
        <h1>All places</h1>

        <Row  >
          {places?.length > 0 ? (
            places.map((place) => {
              return (
                <PlacePreview
                  key={place._id}
                  id={place._id}
                  img={place.image}
                  title={place.title}
                  rating={place.rating}
                  imagesCount={images?.filter((item) => item.image === place.image).length}
                  reviewsCount={reviews?.filter((item) => item.place === place._id).length}
                />
              );
            })
          ) : (
            <h1>There is no places</h1>
          )}
        </Row>
      </div>
    </>
  );
};

export default Home;
