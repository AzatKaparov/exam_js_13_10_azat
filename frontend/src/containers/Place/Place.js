import React, { useEffect, useState } from 'react';
import { API_URL } from '../../constants';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPlace } from '../../store/actions/placesActions';
import { fetchPlaceImages } from '../../store/actions/imagesActions';
import { fetchPlaceReviews } from '../../store/actions/reviewsActions';
import ReviewItem from '../../components/ReviewItem/ReviewItem';
import UploadPhoto from '../../components/UploadPhoto/UploadPhoto';
import './Place.css';

export const getOverallRating = (object) => {
  let sum = 0;

  Object.keys(object).forEach((key) => {
    sum += object[key];
  });

  return parseInt((sum / Object.keys(object).length) * 100) / 100;
};

const Place = ({ match }) => {
  const dispatch = useDispatch();
  const { currentPlace } = useSelector((state) => state.places);
  const { images } = useSelector((state) => state.images);
  const { reviews } = useSelector((state) => state.reviews);
  const [uploaded, setUploaded] = useState(null);
  const [ratings, setRatings] = useState({
    kitchen: null,
    service: null,
    interior: null,
  });

  const getRating = (keyName) => {
    let sum = 0;
    reviews?.forEach((review) => {
      sum += review[keyName];
    });

    return parseInt((sum / reviews.length) * 100) / 100;
  };

  useEffect(() => {
    dispatch(fetchPlaceImages(match.params.id));
  }, [match.params]);

  useEffect(() => {
    dispatch(fetchPlace(match.params.id));
    if (uploaded) {
      dispatch(fetchPlaceImages(match.params    .id));
      setUploaded(false);
    }
    dispatch(fetchPlaceReviews(match.params.id));
  }, [dispatch, match.params.id, images]);

  useEffect(() => {
    if (reviews.length > 0) {
      const newRatingsObject = {};
      Object.keys(ratings).forEach((key) => {
        newRatingsObject[key] = getRating(key);
      });
      setRatings(newRatingsObject);
    }
  }, [reviews]);

  return (
    <div className="container">
      <h1>{currentPlace?.title}</h1>
      <div>
        <img src={`${API_URL}/${currentPlace?.image}`} alt="" />
      </div>
      {currentPlace?.description && <p>{currentPlace.description}</p>}
      <div>
        <h3>Gallery</h3>
        <div className="gallery mb-4">
          {images?.map((image) => (
            <div key={image._id} className="gallery-item">
              <img src={`${API_URL}/${image.image}`} alt={image.image} />
            </div>
          ))}
        </div>
      </div>
      <div className='border p-2 mb-5'>
        <h3>Ratings</h3>
        <ul>
          <li>
            <b>Overall: {reviews.length > 0 ? getOverallRating(ratings) : 'Unavailable'}</b>
          </li>
          <li>Quality of food: {ratings.kitchen}</li>
          <li>Service quality: {ratings.service}</li>
          <li>Interior: {ratings.interior}</li>
        </ul>
      </div>
      <div>
          <h3>Reviews</h3>
        {reviews?.map((review) => (
          <ReviewItem
            key={review._id}
            author={review.author}
            text={review.text}
            date={review.date}
            ratings={{
              kitchen: review.kitchen,
              service: review.service,
              interior: review.interior,
            }}
          />
        ))}
      </div>
      <UploadPhoto id={match.params.id} toggleUpload={() => setUploaded(true)} />
    </div>
  );
};

export default Place;
