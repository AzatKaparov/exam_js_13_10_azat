import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/FormElement/FormElement";
import {getFieldError} from "../../constants";

const Register = () => {
    const dispatch = useDispatch();
    const registerError = useSelector(state => state.users.registerError);
    const [user, setUser] = useState({
        email: "",
        password: "",
        displayName: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };

    return (
        <div className="container">
            <Form onSubmit={submitFormHandler}>
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="email"
                    type="email"
                    value={user.email}
                    placeholder="Email"
                    required
                    label="Email"
                    error={getFieldError(registerError, "email")}
                />
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="displayName"
                    value={user.displayName}
                    type="text"
                    placeholder="Display name"
                    label="Display name"
                    error={getFieldError(registerError, "displayName")}
                    required
                />
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="password"
                    value={user.password}
                    type="password"
                    placeholder="Password"
                    label="Password"
                    error={getFieldError(registerError, "password")}
                    required
                />
                <div className="btns">
                    <Button className="mb-2" variant="primary" type="submit">
                        Submit
                    </Button>
                    <br/>
                </div>
            </Form>
        </div>
    );
};

export default Register;