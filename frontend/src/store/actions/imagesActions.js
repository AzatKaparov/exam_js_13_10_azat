import { axiosApi } from '../../axiosApi';

export const FETCH_IMAGES_REQUEST = 'FETCH_IMAGES_REQUEST';
export const FETCH_IMAGES_SUCCESS = 'FETCH_IMAGES_SUCCESS';
export const FETCH_IMAGES_FAILURE = 'FETCH_IMAGES_FAILURE';

export const fetchImagesRequest = () => ({ type: FETCH_IMAGES_REQUEST });
export const fetchImagesSuccess = (images) => ({ type: FETCH_IMAGES_SUCCESS, payload: images });
export const fetchImagesFailure = (err) => ({ type: FETCH_IMAGES_FAILURE, payload: err });

export const fetchImages = (id) => {
  return async (dispatch) => {
    dispatch(fetchImagesRequest());

    try {
      const response = await axiosApi.get('/images');

      dispatch(fetchImagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchImagesFailure(e));
    }
  };
};

export const FETCH_PLACE_IMAGES_REQUEST = 'FETCH_PLACE_IMAGES_REQUEST';
export const FETCH_PLACE_IMAGES_SUCCESS = 'FETCH_PLACE_IMAGES_SUCCESS';
export const FETCH_PLACE_IMAGES_FAILURE = 'FETCH_PLACE_IMAGES_FAILURE';

export const fetchPlaceImagesRequest = () => ({ type: FETCH_PLACE_IMAGES_REQUEST });
export const fetchPlaceImagesSuccess = (images) => ({
  type: FETCH_PLACE_IMAGES_SUCCESS,
  payload: images,
});
export const fetchPlaceImagesFailure = (err) => ({
  type: FETCH_PLACE_IMAGES_FAILURE,
  payload: err,
});

export const fetchPlaceImages = (id) => {
  return async (dispatch) => {
    dispatch(fetchImagesRequest());

    try {
      const response = await axiosApi.get(`/images/places/${id}`);

      dispatch(fetchImagesSuccess(response.data));
    } catch (e) {
      dispatch(fetchImagesFailure(e));
    }
  };
};

export const ADD_IMAGE_REQUEST = 'ADD_IMAGE_REQUEST';
export const ADD_IMAGE_SUCCESS = 'ADD_IMAGE_SUCCESS';
export const ADD_IMAGE_FAILURE = 'ADD_IMAGE_FAILURE';

export const addImageRequest = () => ({ type: ADD_IMAGE_REQUEST });
export const addImageSuccess = (image) => ({ type: ADD_IMAGE_SUCCESS, payload: image });
export const addImageFailure = (err) => ({ type: ADD_IMAGE_FAILURE, payload: err });

export const addImage = (id, data) => {
  return async (dispatch, getState) => {
    dispatch(addImageRequest());

    try {
      const response = await axiosApi.post(`/images/places/${id}`, data, {
        headers: {
          Authorization: getState().users.user.token,
        },
      });

      dispatch(addImageSuccess(response.data));
    } catch (e) {
      dispatch(addImageFailure(e));
    }
  };
};
