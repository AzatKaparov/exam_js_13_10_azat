import { axiosApi } from '../../axiosApi';
import { historyPush } from './historyActions';

export const FETCH_PLACES_REQUEST = 'FETCH_PLACEs_REQUEST';
export const FETCH_PLACES_SUCCESS = 'FETCH_PLACEs_SUCCESS';
export const FETCH_PLACES_FAILURE = 'FETCH_PLACEs_FAILURE';

export const fetchPlacesRequest = () => ({ type: FETCH_PLACES_REQUEST });
export const fetchPlacesSuccess = (places) => ({ type: FETCH_PLACES_SUCCESS, payload: places });
export const fetchPlacesFailure = (err) => ({ type: FETCH_PLACES_FAILURE, payload: err });

export const fetchPlaces = () => {
  return async (dispatch) => {
    dispatch(fetchPlacesRequest());

    try {
      const response = await axiosApi.get('/places');

      dispatch(fetchPlacesSuccess(response.data));
    } catch (e) {
      dispatch(fetchPlacesFailure(e));
    }
  };
};

export const FETCH_PLACE_REQUEST = 'FETCH_PLACE_REQUEST';
export const FETCH_PLACE_SUCCESS = 'FETCH_PLACE_SUCCESS';
export const FETCH_PLACE_FAILURE = 'FETCH_PLACE_FAILURE';

export const fetchPlaceRequest = () => ({ type: FETCH_PLACE_REQUEST });
export const fetchPlaceSuccess = (place) => ({ type: FETCH_PLACE_SUCCESS, payload: place });
export const fetchPlaceFailure = (err) => ({ type: FETCH_PLACE_FAILURE, payload: err });

export const fetchPlace = (id) => {
  return async (dispatch) => {
    dispatch(fetchPlacesRequest());

    try {
      const response = await axiosApi.get(`/places/${id}`);

      dispatch(fetchPlaceSuccess(response.data));
    } catch (e) {
      dispatch(fetchPlaceFailure(e));
    }
  };
};

export const CREATE_PLACE_REQUEST = 'CREATE_PLACES_REQUEST';
export const CREATE_PLACE_SUCCESS = 'CREATE_PLACES_SUCCESS';
export const CREATE_PLACE_FAILURE = 'CREATE_PLACES_FAILURE';

export const createPlaceRequest = () => ({ type: CREATE_PLACE_REQUEST });
export const createPlaceSuccess = (place) => ({ type: CREATE_PLACE_SUCCESS, payload: place });
export const createPlaceFailure = (err) => ({ type: CREATE_PLACE_FAILURE, payload: err });

export const createPlace = (placeData) => {
  return async (dispatch, getState) => {
    dispatch(createPlaceRequest());

    try {
      await axiosApi.post('/places', placeData, {
        headers: {
          Authorization: getState().users.user.token,
        },
      });
      dispatch(createPlaceSuccess());
      dispatch(historyPush(`/`));
    } catch (e) {
      if (e.response.data) {
        dispatch(createPlaceFailure(e.response.data));
      }
    }
  };
};

export const DELETE_PLACE_REQUEST = 'DELETE_PLACE_REQUEST';
export const DELETE_PLACE_SUCCESS = 'DELETE_PLACE_SUCCESS';
export const DELETE_PLACE_FAILURE = 'DELETE_PLACE_FAILURE';

export const deletePlaceRequest = () => ({ type: DELETE_PLACE_REQUEST });
export const deletePlaceSuccess = (place) => ({ type: DELETE_PLACE_SUCCESS, payload: place });
export const deletePlaceFailure = (err) => ({ type: DELETE_PLACE_FAILURE, payload: err });

export const deletePlace = (id) => {
  return async (dispatch, getState) => {
    dispatch(deletePlaceRequest());

    try {
      const response = await axiosApi.delete(`/places/${id}`, {
        headers: {
          Authorization: getState().users.user.token,
        },
      });
      dispatch(deletePlaceSuccess(response.data));
    } catch (e) {
      dispatch(deletePlaceFailure(e));
    }
  };
};
