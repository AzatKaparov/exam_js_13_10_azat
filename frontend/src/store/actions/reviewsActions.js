import { axiosApi } from '../../axiosApi';

export const FETCH_REVIEWS_REQUEST = 'FETCH_REVIEWS_REQUEST';
export const FETCH_REVIEWS_SUCCESS = 'FETCH_REVIEWS_SUCCESS';
export const FETCH_REVIEWS_FAILURE = 'FETCH_REVIEWS_FAILURE';

export const fetchReviewsRequest = () => ({ type: FETCH_REVIEWS_REQUEST });
export const fetchReviewsSuccess = (reviews) => ({ type: FETCH_REVIEWS_SUCCESS, payload: reviews });
export const fetchReviewsFailure = (err) => ({ type: FETCH_REVIEWS_FAILURE, payload: err });

export const fetchReviews = () => {
  return async (dispatch) => {
    dispatch(fetchReviewsRequest());

    try {
      const response = await axiosApi.get('/reviews');

      dispatch(fetchReviewsSuccess(response.data));
    } catch (e) {
      dispatch(fetchReviewsFailure(e));
    }
  };
};

export const FETCH_PLACE_REVIEWS_REQUEST = 'FETCH_PLACE_REVIEWS_REQUEST';
export const FETCH_PLACE_REVIEWS_SUCCESS = 'FETCH_PLACE_REVIEWS_SUCCESS';
export const FETCH_PLACE_REVIEWS_FAILURE = 'FETCH_PLACE_REVIEWS_FAILURE';

export const fetchPlaceReviewsRequest = () => ({ type: FETCH_PLACE_REVIEWS_REQUEST });
export const fetchPlaceReviewsSuccess = (reviews) => ({
  type: FETCH_PLACE_REVIEWS_SUCCESS,
  payload: reviews,
});
export const fetchPlaceReviewsFailure = (err) => ({
  type: FETCH_PLACE_REVIEWS_FAILURE,
  payload: err,
});

export const fetchPlaceReviews = (id) => {
  return async (dispatch) => {
    dispatch(fetchReviewsRequest());

    try {
      const response = await axiosApi.get(`/reviews/places/${id}`);

      dispatch(fetchReviewsSuccess(response.data));
    } catch (e) {
      dispatch(fetchReviewsFailure(e));
    }
  };
};
