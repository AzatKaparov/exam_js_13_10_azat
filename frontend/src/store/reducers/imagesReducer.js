import {
  FETCH_IMAGES_FAILURE,
  FETCH_IMAGES_REQUEST,
  FETCH_IMAGES_SUCCESS,
  FETCH_PLACE_IMAGES_REQUEST,
  FETCH_PLACE_IMAGES_SUCCESS,
  FETCH_PLACE_IMAGES_FAILURE,
  ADD_IMAGE_REQUEST,
  ADD_IMAGE_SUCCESS,
  ADD_IMAGE_FAILURE,
} from '../actions/imagesActions';

const initialState = {
  imagesLoading: false,
  imagesError: null,
  images: [],
};

const imagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_IMAGES_REQUEST:
      return { ...state, imagesLoading: true };
    case FETCH_IMAGES_SUCCESS:
      return { ...state, images: action.payload, imagesLoading: false, imagesError: null };
    case FETCH_IMAGES_FAILURE:
      return { ...state, images: state.images, imagesLoading: false, imagesError: action.payload };
    case FETCH_PLACE_IMAGES_REQUEST:
      return { ...state, imagesLoading: true };
    case FETCH_PLACE_IMAGES_SUCCESS:
      return { ...state, images: action.payload, imagesLoading: false, imagesError: null };
    case FETCH_PLACE_IMAGES_FAILURE:
      return { ...state, images: state.images, imagesLoading: false, imagesError: action.payload };
    case ADD_IMAGE_REQUEST:
      return { ...state, imagesLoading: true };
    case ADD_IMAGE_SUCCESS:
      return {
        ...state,
        images: [...state.images, action.payload],
        imagesLoading: false,
        imagesError: null,
      };
    case ADD_IMAGE_FAILURE:
      return { ...state, images: state.images, imagesLoading: false, imagesError: action.payload };
    default:
      return state;
  }
};

export default imagesReducer;
