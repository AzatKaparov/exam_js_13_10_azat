import {
  CREATE_PLACE_FAILURE,
  CREATE_PLACE_REQUEST,
  CREATE_PLACE_SUCCESS,
  DELETE_PLACE_FAILURE,
  DELETE_PLACE_REQUEST,
  DELETE_PLACE_SUCCESS,
  FETCH_PLACES_FAILURE,
  FETCH_PLACES_REQUEST,
  FETCH_PLACES_SUCCESS,
  FETCH_PLACE_FAILURE,
  FETCH_PLACE_REQUEST,
  FETCH_PLACE_SUCCESS,
} from '../actions/placesActions';

const initialState = {
  placesLoading: false,
  placesError: null,
  places: [],
  currentPlace: {},
};

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PLACES_REQUEST:
      return { ...state, placesLoading: true };
    case FETCH_PLACES_SUCCESS:
      return { ...state, places: action.payload, placesLoading: false, placesError: null };
    case FETCH_PLACES_FAILURE:
      return { ...state, places: [], placesLoading: false, placesError: action.payload };
    case DELETE_PLACE_REQUEST:
      return { ...state, placesLoading: true };
    case DELETE_PLACE_SUCCESS:
      const filtered = state.places.filter((item) => item._id !== action.payload._id);
      return { ...state, placesLoading: false, places: filtered };
    case DELETE_PLACE_FAILURE:
      return { ...state, placesLoading: false, placesError: action.payload };
    case CREATE_PLACE_REQUEST:
      return { ...state, placesLoading: true };
    case CREATE_PLACE_SUCCESS:
      return { ...state, placesLoading: false, placesError: null };
    case CREATE_PLACE_FAILURE:
      return { ...state, placesLoading: false, placesError: action.payload };
    case FETCH_PLACE_REQUEST:
      return { ...state, placesLoading: true };
    case FETCH_PLACE_SUCCESS:
      return { ...state, placesLoading: false, currentPlace: action.payload };
    case FETCH_PLACE_FAILURE:
      return { ...state, placesLoading: false, placesError: action.payload };
    default:
      return state;
  }
};

export default placesReducer;
