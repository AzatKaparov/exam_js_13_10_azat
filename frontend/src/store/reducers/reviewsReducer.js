import {
  FETCH_REVIEWS_FAILURE,
  FETCH_REVIEWS_REQUEST,
  FETCH_REVIEWS_SUCCESS,
  FETCH_PLACE_REVIEWS_REQUEST,
  FETCH_PLACE_REVIEWS_SUCCESS,
  FETCH_PLACE_REVIEWS_FAILURE,
} from '../actions/reviewsActions';

const initialState = {
  reviewsLoading: false,
  reviewsError: null,
  reviews: [],
};

const reviewsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REVIEWS_REQUEST:
      return { ...state, reviewsLoading: true };
    case FETCH_REVIEWS_SUCCESS:
      return { ...state, reviews: action.payload, reviewsLoading: false, reviewsError: null };
    case FETCH_REVIEWS_FAILURE:
      return { ...state, reviews: [], reviewsLoading: false, reviewsError: action.payload };
    case FETCH_PLACE_REVIEWS_REQUEST:
      return { ...state, reviewsLoading: true };
    case FETCH_PLACE_REVIEWS_SUCCESS:
      return { ...state, reviews: action.payload, reviewsLoading: false, reviewsError: null };
    case FETCH_PLACE_REVIEWS_FAILURE:
      return { ...state, reviews: [], reviewsLoading: false, reviewsError: action.payload };
    default:
      return state;
  }
};

export default reviewsReducer;
